const Vigenere = require('caesar-salad').Vigenere;
const password = 'weddk';

const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Enter your word\n');
});

app.get('/:password/encode/:crypt', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(`${req.params.crypt}`));
});

app.get('/:password/decode/:crypt', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(`${req.params.crypt}`));
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});


